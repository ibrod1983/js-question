const fs = require('fs')

function main() {
    try {
      getFileData('./someNonExistingFile.txt', (data) => {
        console.log('Data from main(): ',data);
    });  
    } catch (error) {
        console.log('Caught error in main(): ',error, "Throwing it to the caller.")
        throw error;
    }
    
}

function getFileData(path, cb) {
    fs.readFile(path, (err, data) => {
        if (err) {//If the file doesn't exist, an exception is thrown.
            throw err
        }
        cb(data)
    })
}

try {
   main() 
} catch (error) {
    console.log('Error caught in upper-most code: ',error)
}
