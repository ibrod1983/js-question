const fs = require('fs')



async function main() {
    try {
        const data = await getFileData('./someNonExistingFile.txt');
        console.log('data from main(): ', data)
    } catch (error) {      
        console.log('Caught error in main(): ', error, "Throwing it to the caller.")
        throw error;
    }

}

function getFileData(path, cb) {
    return new Promise((res, rej) => {
        fs.readFile(path, (err, data) => {
            if (err) {          
                rej(err)
            }
            res(data)
        })
    })}


    main().catch(e=>console.log('Error caught in upper-most code: ',e))
  

