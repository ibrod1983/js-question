## Run the code via node

### Problem:
What would be the outcome of running the code in problem.js? What will be shown in the console?

### Explanation needed:
Explain why it behaves the way it does.

### Possible solution:
Make minimalistic changes to the code, that will produce this result: Both console.logs are printed on the screen. An example solution can be found in solution.js